var minhaVar = 'minha variavel';

function minhaFunc(x, y) {
    return x+y;
}

//ES 6 (2015)
let num = 2;
const PI = 3.14;

var numeros = [1, 2, 3];
numeros.map(function(valor) { //JS PURO
    return valor * 2;
});

numeros.map( valor => valor*2 ); //ES 6

class Matematica {

    soma(x, y) {
        return x + y;
    }
}

var tipagem: string = 'asdf';
//tipagem = 4;  //<- Isso não é permitido pois a variável é do tipo string