import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  url = 'http://loiane.training';
  angular = true;
  urlImage1 = 'http://lorempixel.com/400/200/nature/';
  urlImage2 = 'http://lorempixel.com/400/200/cats/';

  valorAtual: string;
  valorSalvo: string;

  isMouseOver: Boolean = true;

  nome: String = 'abc';

  constructor() { }

  getValor() {
    return 1;
  }

  getCurtirCurso() {
    return true;
  }

  onKeyUp(evento: KeyboardEvent) {
    this.valorAtual = (<HTMLInputElement>evento.target).value;
  }

  onEnter(value) {
    this.valorSalvo = value;
  }

  onMouseOverOut() {
    this.isMouseOver = !this.isMouseOver;
  }

  ngOnInit() {
  }
}
